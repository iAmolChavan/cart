//
//  ProductDetailsView.m
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import "ProductDetailsView.h"

#import "CartView.h"

#import "ImageHelper.h"
#import "Cart.h"
#import "UIBarButtonItem+Badge.h"

#import "YSLTransitionAnimator.h"
#import "UIViewController+YSLTransition.h"

@interface ProductDetailsView ()<YSLTransitionAnimatorDataSource>

{
    int selectedQuantity;
    
    Cart *cart;
}
@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
- (IBAction)changeImage:(id)sender;
@end

@implementation ProductDetailsView

@synthesize selectedProduct;

@synthesize productImageView, productNameLabel, productPriceLabel, productQuantityLabel, quantityStepper, btnAddToCart;

# pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    cart = [Cart sharedInstance];
    
    [self configureNavigationBar];
    
    
    [productImageView setUserInteractionEnabled:YES];
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];

    // Adding the swipe gesture on image view
    [productImageView addGestureRecognizer:swipeLeft];
    [productImageView addGestureRecognizer:swipeRight];
    
    
    productImageView.image = [UIImage imageNamed:[selectedProduct.imagePath firstObject]];
    
    productImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    productNameLabel.text = selectedProduct.name;
    productPriceLabel.text = [NSString stringWithFormat:@"₹ %0.2f", selectedProduct.price];
    _pageController.numberOfPages = selectedProduct.imagePath.count;
    
     self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[cart.cartItems count]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    productQuantityLabel.text = [NSString stringWithFormat:@"Quantity : %i", selectedProduct.quantities];
    
    selectedQuantity = selectedProduct.quantities;
    
    quantityStepper.value = selectedQuantity;
    
    if ([self isViewLoadedFromCartView])
        btnAddToCart.enabled = false;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self ysl_removeTransitionDelegate];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self ysl_addTransitionDelegate:self];
     [self ysl_popTransitionAnimationWithCurrentScrollView:nil
                                     cancelAnimationPointY:0
                                         animationDuration:0.7
                                   isInteractiveTransition:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

# pragma mark - Private Methods

- (BOOL)isViewLoadedFromCartView
{
    if ([[[self.navigationController viewControllers] objectAtIndex:[[self.navigationController viewControllers] count] - 2] class] == [CartView class])
        return YES;
    else
        return NO;
}

- (void)configureNavigationBar
{
    self.title = selectedProduct.name;
    
    if ([self isViewLoadedFromCartView] == NO)
    {
        UIImage *applicationLogoImage = [ImageHelper imageWithImage:[UIImage imageNamed:@"Cart"] scaleToSize:CGSizeMake(35, 35)];
        
        applicationLogoImage = [applicationLogoImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        UIButton *cartButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cartButton.frame = CGRectMake(0, 0, 35, 35);
        [cartButton setImage:applicationLogoImage forState:UIControlStateNormal];
        [cartButton addTarget:self action:@selector(cartTapped:) forControlEvents:UIControlEventTouchUpInside];

        
        cartButton.layer.masksToBounds = YES;
        cartButton.layer.cornerRadius = 5.0f;
        
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton];
        
        self.navigationItem.rightBarButtonItem = rightBarButton;
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
}

# pragma mark - Actions

- (IBAction)cartTapped:(id)sender
{
    [self performSegueWithIdentifier:@"CartView" sender:self];
}

- (IBAction)btnAddToCartTapped:(id)sender
{
    selectedProduct.quantities = selectedQuantity;
    
    [cart addCartItem:selectedProduct];
    
    btnAddToCart.enabled = false;
    
    // this is the key entry to change the badgeValue.
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[cart.cartItems count]];
    
}

- (IBAction)productQuantityChanged:(id)sender
{
    double value = [(UIStepper *)sender value];
    
    selectedQuantity = (int)value;
    
    productQuantityLabel.text = [NSString stringWithFormat:@"Quantity : %d", selectedQuantity];
    
    if (selectedQuantity == selectedProduct.quantities)
    {
        btnAddToCart.enabled = false;
    }
    else
    {
        btnAddToCart.enabled = true;
    }
    
}

# pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}


- (IBAction)changeImage:(id)sender {
    NSLog(@"%li",([self.pageController currentPage]+1));
    
    [self swipeImage:(int)[self.pageController currentPage]];
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
        self.pageController.currentPage -=1;
    }
    else if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"Right Swipe");
        self.pageController.currentPage +=1;
        
    }
        [self swipeImage:(int)[self.pageController currentPage]];
}

-(void)swipeImage:(int)pageController{
    if (pageController>=0 && pageController<[selectedProduct.imagePath count]) {
        productImageView.image = [UIImage imageNamed:[selectedProduct.imagePath objectAtIndex:(long)pageController]];
    }
}

#pragma mark -- YSLTransitionAnimatorDataSource

- (UIImageView *)pushTransitionImageView
{
    return  productImageView;
}

- (UIImageView *)popTransitionImageView
{
    return self.productImageView;
}
@end