//
//  ProductsList.m
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import "ProductsList.h"

NSString *kProducts = @"Products";

static ProductsList *productsList = nil;


@implementation ProductsList

@synthesize products, productTypes;

+ (instancetype)sharedInstance
{
    @synchronized(self)
    {
        if (productsList == nil)
        {
            productsList = [ProductsList new];
            [productsList setProductsList];
        }
    }
    return productsList;
}

/**
 * @brief List of products is created if first time application in launched, otherwise it will use saved products
 */
- (void)setProductsList
{
    products = [NSMutableArray new];
    productTypes = [NSMutableArray new];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ProductList" ofType:@"json"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    NSError *error =  nil;
    NSDictionary *jsonDataArray = [NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    for (id obj in jsonDataArray) {
        [products addObject:[self getArray:[jsonDataArray objectForKey:obj] wityType:[self getProductTypeEquivalentForString:obj]]];
        [productTypes addObject:obj];
    }
    

}


/**
 * @brief Generates Unique identifier for each product
 */
- (NSString*)generateUniqueId
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    NSString *strUUID = (__bridge NSString *)string;
    return strUUID;
}

/**
 * @brief String to ProductType
 */
- (ProductType)getProductTypeEquivalentForString:(NSString *)strProductType
{
    if ([strProductType isEqualToString:@"Electronics"])
    {
        return ELECTRONICS;
    }
    else if ([strProductType isEqualToString:@"Furniture"])
    {
        return FURNITURE;
    }
    else
    {
        return OTHER;
    }
}

/**
 * @brief Get Array from Json
 */
-(NSArray*)getArray:(NSDictionary*)dict wityType:(ProductType)type{
    NSLog(@"%@",dict);
    NSMutableArray *arr = [NSMutableArray new];
    for (id obj in dict) {
        NSLog(@"%@",obj);
        Product *objProduct = [Product new];
        objProduct.name = [obj objectForKey:@"name"];
        objProduct.price = [[obj objectForKey:@"price"] floatValue];
        objProduct.quantities = [[obj objectForKey:@"quantities"] intValue];
        objProduct.Id = [self generateUniqueId];
        objProduct.imagePath = [obj objectForKey:@"image"];
        objProduct.productType = type;
        [arr addObject:objProduct];
        
    }
    return arr;
}

@end