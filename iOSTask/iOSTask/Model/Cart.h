//
//  Cart.h
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Product.h"

@interface Cart : NSObject

@property NSMutableArray *cartItems;

+ (instancetype)sharedInstance;

- (void)addCartItem:(Product *)cartItem;

- (void)removeCartItem:(Product *)cartItem;

- (void)resetCartItems;

- (float)getTotalAmount;

- (int)getTotalItems;

-(void)getCartInfo;

@end
